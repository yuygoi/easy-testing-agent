package com.franklin.ideaplugin.easytesting.controllerclient.request;

import com.franklin.ideaplugin.easytesting.controllerclient.beans.MethodData;
import com.franklin.ideaplugin.easytesting.controllerclient.beans.RequestData;

/**
 * 构建请求基本数据
 *
 * @author Ye Junhui
 * @since 2023/6/29
 */
public interface IRequestBuilder {

    /**
     * 构建请求数据
     * @param methodData
     * @param parameterNames
     * @param args
     * @return
     */
    RequestData buildRequest(MethodData methodData, String[] parameterNames, Object[] args);
}
