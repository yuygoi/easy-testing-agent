package com.franklin.ideaplugin.easytesting.controllerclient;

import com.franklin.ideaplugin.easytesting.controllerclient.client.IControllerClientBuilder;
import com.franklin.ideaplugin.easytesting.controllerclient.utils.ControllerUtil;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * @author Ye Junhui
 * @since 2023/6/29
 */
@RequiredArgsConstructor
public class ControllerClientFactoryBean implements FactoryBean<Object> {

    private final Class<?> controllerClazz;
    private final Class<?> proxyClazz;
    private final ApplicationContext applicationContext;


    @Override
    public Object getObject(){
        IControllerClientBuilder controllerClientBuilder = applicationContext.getBean(IControllerClientBuilder.class);
        return controllerClientBuilder.newInstance(controllerClazz,proxyClazz);
    }

    @Override
    public Class<?> getObjectType() {
        return this.proxyClazz;
    }

}
