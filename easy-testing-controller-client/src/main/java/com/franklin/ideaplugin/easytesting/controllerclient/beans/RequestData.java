package com.franklin.ideaplugin.easytesting.controllerclient.beans;

import lombok.Data;
import org.springframework.http.HttpMethod;
import org.springframework.lang.Nullable;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Ye Junhui
 * @since 2023/6/27
 */
@Data
public class RequestData {

    /**
     * 请求路径
     */
    private String url;

    /**
     * 请求方法
     */
    private HttpMethod httpMethod;

    /**
     * 请求头
     */
    private MultiValueMap<String,String> httpHeaders = new LinkedMultiValueMap<>();

    /**
     * form-data参数
     */
    private MultiValueMap<String,String> formData = new LinkedMultiValueMap<>();

    /**
     * 文件
     */
    private MultiValueMap<String,String> fileData = new LinkedMultiValueMap<>();

    /**
     * 路径参数
     */
    private Map<String,String> pathParams = new LinkedHashMap<>();

    /**
     * 查询参数
     */
    private Map<String,String> queryParams = new LinkedHashMap<>();

    /**
     * JSON请求体
     */
    @Nullable
    private Object requestBody;
}
