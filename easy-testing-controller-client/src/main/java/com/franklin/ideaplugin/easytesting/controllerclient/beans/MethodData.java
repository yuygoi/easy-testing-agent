package com.franklin.ideaplugin.easytesting.controllerclient.beans;

import com.franklin.ideaplugin.easytesting.controllerclient.request.RequestPosition;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.*;

/**
 * 方法基本信息
 *
 * @author Ye Junhui
 * @since 2023/6/28
 */
@Data
@RequiredArgsConstructor
public class MethodData implements Serializable {

    private final Method method;

    /**
     * 参数在请求中的位置
     */
    private Map<Integer, RequestPosition> paramPositions = new HashMap<>();

    /**
     * 参数在请求中的key
     */
    private Map<Integer,String> paramKeys = new HashMap<>();

    /**
     * 请求方法
     */
    private HttpMethod httpMethod;

    /**
     * 方法请求头
     */
    private MultiValueMap<String,String> methodHeaders = new LinkedMultiValueMap<>();

    /**
     * 请求路径
     */
    private String url;
}
