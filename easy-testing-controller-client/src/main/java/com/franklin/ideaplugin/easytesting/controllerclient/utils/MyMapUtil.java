package com.franklin.ideaplugin.easytesting.controllerclient.utils;

import cn.hutool.core.collection.CollectionUtil;
import org.springframework.util.MultiValueMap;

import java.util.function.BiConsumer;

/**
 * @author Ye Junhui
 * @since 2023/6/29
 */
public abstract class MyMapUtil {

    /**
     * 深度边里
     * @param multiValueMap
     * @param consumer
     * @param <K>
     * @param <V>
     */
    public static <K,V> void forEachDeep(MultiValueMap<K,V> multiValueMap, BiConsumer<K,V> consumer){
        if (CollectionUtil.isEmpty(multiValueMap)){
            return;
        }
        multiValueMap.forEach((k,values) -> {
            values.forEach(v -> consumer.accept(k,v));
        });
    }
}
