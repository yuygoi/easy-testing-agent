package com.franklin.ideaplugin.easytesting.controllerclient.request;

import com.franklin.ideaplugin.easytesting.controllerclient.beans.MethodData;
import com.franklin.ideaplugin.easytesting.controllerclient.beans.RequestData;

/**
 * 请求执行器
 *
 * @author Ye Junhui
 * @since 2023/6/29
 */
public interface IRequestInvoker {

    /**
     * 执行请求
     * @param methodData
     * @param requestData
     * @param targetUrl
     * @return
     */
    Object invoke(MethodData methodData, RequestData requestData,String targetUrl);
}
