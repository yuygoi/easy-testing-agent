package com.franklin.ideaplugin.easytesting.controllerclient.request;

import com.franklin.ideaplugin.easytesting.controllerclient.beans.RequestData;

/**
 * 请求数据填充器
 *
 * @author Ye Junhui
 * @since 2023/6/29
 */
interface IRequestFiller {

    /**
     * 填充请求数据
     * @param requestData
     * @param parameterName
     * @param parameterType
     * @param arg
     * @return
     */
    boolean fill(RequestData requestData,String parameterName,Class<?> parameterType,Object arg);
}
