package com.franklin.ideaplugin.easytesting.controllerclient.annotation;

import cn.hutool.core.util.StrUtil;
import com.franklin.ideaplugin.easytesting.controllerclient.IMethodParameterProcessor;
import com.franklin.ideaplugin.easytesting.controllerclient.beans.MethodData;
import com.franklin.ideaplugin.easytesting.controllerclient.request.RequestPosition;
import org.springframework.web.bind.annotation.RequestParam;

import java.lang.annotation.Annotation;
import java.util.Objects;

/**
 * @author Ye Junhui
 * @since 2023/6/28
 */
public class RequestParamParameterProcessor implements IMethodParameterProcessor {

    private final static Class<RequestParam> ANNOTATION = RequestParam.class;

    @Override
    public Class<? extends Annotation> getAnnotationType() {
        return ANNOTATION;
    }

    @Override
    public boolean processParameter(MethodData methodData, Annotation annotation, Integer parameterIndex, Class<?> parameterType) {
        if (Objects.isNull(annotation) || !annotation.annotationType().isAssignableFrom(ANNOTATION)){
            return false;
        }
        RequestParam requestParam = ANNOTATION.cast(annotation);
        methodData.getParamPositions().put(parameterIndex, RequestPosition.FORM_DATA);
        if (StrUtil.isNotBlank(requestParam.value())){
            methodData.getParamKeys().put(parameterIndex, requestParam.value());
        }else if (StrUtil.isNotBlank(requestParam.name())){
            methodData.getParamKeys().put(parameterIndex, requestParam.name());
        }else {
            methodData.getParamKeys().put(parameterIndex, parameterIndex.toString());
        }
        return true;
    }

}
