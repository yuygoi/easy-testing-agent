package com.franklin.ideaplugin.easytesting.controllerclient;

import com.franklin.ideaplugin.easytesting.controllerclient.beans.MethodData;
import org.springframework.lang.Nullable;

import java.lang.annotation.Annotation;

/**
 * 方法参数-处理器
 *
 * @author Ye Junhui
 * @since 2023/6/28
 */
public interface IMethodParameterProcessor {

    @Nullable
    Class<? extends Annotation> getAnnotationType();

    /**
     * 处理请求参数到元数据中
     *
     * @param annotation
     * @param parameterIndex
     * @param parameterType
     * @return
     */
    boolean processParameter(
            MethodData methodData,
            Annotation annotation,
            Integer parameterIndex,
            Class<?> parameterType
    );
}
