package com.franklin.ideaplugin.easytesting.controllerclient.annotation;

import com.franklin.ideaplugin.easytesting.controllerclient.IMethodParameterProcessor;
import com.franklin.ideaplugin.easytesting.controllerclient.beans.MethodData;
import com.franklin.ideaplugin.easytesting.controllerclient.request.RequestPosition;
import org.springframework.web.bind.annotation.RequestBody;

import java.lang.annotation.Annotation;
import java.util.Objects;

/**
 * @author Ye Junhui
 * @since 2023/6/28
 */
public class RequestBodyParameterProcessor implements IMethodParameterProcessor {

    private final static Class<RequestBody> ANNOTATION = RequestBody.class;

    @Override
    public Class<? extends Annotation> getAnnotationType() {
        return ANNOTATION;
    }

    @Override
    public boolean processParameter(MethodData methodData, Annotation annotation, Integer parameterIndex, Class<?> parameterType) {
        if (Objects.isNull(annotation) || !annotation.annotationType().isAssignableFrom(ANNOTATION)){
            return false;
        }
        RequestBody requestBody = ANNOTATION.cast(annotation);
        methodData.getParamPositions().put(parameterIndex, RequestPosition.BODY);
        return true;
    }

}
