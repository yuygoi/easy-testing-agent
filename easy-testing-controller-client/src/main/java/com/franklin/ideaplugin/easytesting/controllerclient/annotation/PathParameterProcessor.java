package com.franklin.ideaplugin.easytesting.controllerclient.annotation;

import cn.hutool.core.util.StrUtil;
import com.franklin.ideaplugin.easytesting.controllerclient.IMethodParameterProcessor;
import com.franklin.ideaplugin.easytesting.controllerclient.beans.MethodData;
import com.franklin.ideaplugin.easytesting.controllerclient.request.RequestPosition;
import org.springframework.web.bind.annotation.PathVariable;

import java.lang.annotation.Annotation;
import java.util.Objects;

/**
 * @author Ye Junhui
 * @since 2023/6/28
 */
public class PathParameterProcessor implements IMethodParameterProcessor {

    private final static Class<PathVariable> ANNOTATION = PathVariable.class;

    @Override
    public Class<? extends Annotation> getAnnotationType() {
        return ANNOTATION;
    }

    @Override
    public boolean processParameter(MethodData methodData, Annotation annotation, Integer parameterIndex, Class<?> parameterType) {
        if (Objects.isNull(annotation) || !annotation.annotationType().isAssignableFrom(ANNOTATION)){
            return false;
        }
        PathVariable pathVariable = ANNOTATION.cast(annotation);
        methodData.getParamPositions().put(parameterIndex, RequestPosition.PATH);
        if (StrUtil.isNotBlank(pathVariable.value())){
            methodData.getParamKeys().put(parameterIndex, pathVariable.value());
        }else if (StrUtil.isNotBlank(pathVariable.name())){
            methodData.getParamKeys().put(parameterIndex, pathVariable.name());
        }else {
            methodData.getParamKeys().put(parameterIndex, parameterIndex.toString());
        }
        return true;
    }

}
