package com.franklin.ideaplugin.easytesting.controllerclient.client;

/**
 * @author Ye Junhui
 * @since 2023/6/28
 */
public interface IControllerClientBuilder {

    /**
     * 构建代理对象
     *
     * @param controllerClazz
     * @param proxyClazz
     * @return
     */
    Object newInstance(Class<?> controllerClazz,Class<?> proxyClazz);
}
