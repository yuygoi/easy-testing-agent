package com.franklin.ideaplugin.easytesting.controllerclient.invoke;


import com.franklin.ideaplugin.easytesting.common.entity.MethodInvokeData;

import java.lang.reflect.Method;

/**
 * @author Ye Junhui
 * @since 2023/6/29
 */
public interface IControllerMethodInvocationHandler {

    /**
     * 执行方法
     * @param proxy
     * @param method
     * @param methodInvokeData
     * @param targetUrl
     * @param originMethod
     * @param args
     * @return
     * @throws Throwable
     */
    Object invoke(
            Object proxy,
            Method method,
            MethodInvokeData methodInvokeData,
            String targetUrl,
            Method originMethod,
            Object[] args
    ) throws Throwable;
}
