package com.franklin.ideaplugin.easytesting.controllerclient;

import com.franklin.ideaplugin.easytesting.common.utils.ReflectionUtils;
import com.franklin.ideaplugin.easytesting.controllerclient.beans.MethodData;
import com.franklin.ideaplugin.easytesting.controllerclient.request.RequestPosition;
import com.franklin.ideaplugin.easytesting.controllerclient.utils.ControllerUtil;

import java.lang.annotation.Annotation;
import java.util.Objects;

/**
 * @author Ye Junhui
 * @since 2023/6/28
 */
public class NotAnnotatedParameterProcessor implements IMethodParameterProcessor {

    @Override
    public Class<? extends Annotation> getAnnotationType() {
        return null;
    }

    @Override
    public boolean processParameter(MethodData methodData, Annotation annotation, Integer parameterIndex, Class<?> parameterType) {
        if (Objects.nonNull(annotation)){
            return false;
        }

        methodData.getParamPositions().put(parameterIndex, RequestPosition.FORM_DATA);
        if (ReflectionUtils.isBasicType(parameterType)){
            methodData.getParamKeys().put(parameterIndex, parameterIndex.toString());
        }else {
            String canonicalName = parameterType.getCanonicalName();
            if (ControllerUtil.isHttpRequestClass(canonicalName)){
                return false;
            }else if (ControllerUtil.isHttpResponseClass(canonicalName)){
                return false;
            } else{
                methodData.getParamKeys().put(parameterIndex,parameterIndex.toString());
            }
        }
        return true;
    }

}
