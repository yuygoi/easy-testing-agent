package com.franklin.ideaplugin.easytesting.controllerclient.beans;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 文本范围
 *
 * @author Ye Junhui
 * @since 2023/6/28
 */
@Getter
@AllArgsConstructor
public class TextRange {

    /**
     * 左索引
     */
    private final int leftIndex;

    /**
     * 右索引
     */
    private final int rightIndex;
}
