package com.franklin.ideaplugin.easytesting.controllerclient.utils;

import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Objects;

/**
 * @author Ye Junhui
 * @since 2023/6/26
 */
public class MvcAnnotationUtils {

    /**
     * 获取requestMapping的第一个路径
     * @param requestMapping
     * @return
     */
    public static String getFirstPath(RequestMapping requestMapping){
        if (Objects.isNull(requestMapping)){
            return "";
        }
        String[] value = requestMapping.value();
        String[] pathArr = requestMapping.path();
        String path = "";
        if (value.length > 0) {
            path = value[0];
        } else if (pathArr.length > 0) {
            path = pathArr[0];
        }
        return path;
    }
}
