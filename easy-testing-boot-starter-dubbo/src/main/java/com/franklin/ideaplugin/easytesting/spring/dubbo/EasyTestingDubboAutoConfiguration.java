package com.franklin.ideaplugin.easytesting.spring.dubbo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Ye Junhui
 * @since 2023/7/13
 */
@Configuration
public class EasyTestingDubboAutoConfiguration {

    @Bean
    public EasyTestingReferenceAnnotationBeanRegistrar easyTestingReferenceAnnotationBeanPostProcessor(){
        return new EasyTestingReferenceAnnotationBeanRegistrar();
    }

}
