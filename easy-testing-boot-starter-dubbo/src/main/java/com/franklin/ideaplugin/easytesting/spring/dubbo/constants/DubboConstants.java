package com.franklin.ideaplugin.easytesting.spring.dubbo.constants;

/**
 * @author Ye Junhui
 * @since 2023/7/13
 */
public interface DubboConstants {

    /**
     * alibaba-dubbo
     */
    String ALIBABA_DUBBO_ANN_TYPE = "com.alibaba.dubbo.config.annotation.Reference";
    String ALIBABA_DUBBO_BEAN_POST_PROCESSOR_TYPE = "com.alibaba.dubbo.config.spring.beans.factory.annotation.ReferenceAnnotationBeanPostProcessor";

    /**
     * apache-dubbo
     */
    String APACHE_DUBBO_ANN_TYPE = "org.apache.dubbo.config.annotation.Reference";
    String APACHE_DUBBO_BEAN_POST_PROCESSOR_TYPE = "org.apache.dubbo.config.spring.beans.factory.annotation.ReferenceAnnotationBeanPostProcessor";

    /**
     * bean后置处理器bean名称
     */
    String BEAN_POST_PROCESSOR__BEAN_NAME = "referenceAnnotationBeanPostProcessor";
}
