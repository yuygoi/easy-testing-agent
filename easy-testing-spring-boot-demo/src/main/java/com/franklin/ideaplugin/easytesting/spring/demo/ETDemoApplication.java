package com.franklin.ideaplugin.easytesting.spring.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Ye Junhui
 * @since 2023/5/15
 */
@SpringBootApplication
public class ETDemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(ETDemoApplication.class,args);
    }
}
