package com.franklin.ideaplugin.easytesting.spring.demo.constants;

/**
 * @author Ye Junhui
 * @since 2023/5/25
 */
public enum DemoEnum {

    INSTANCE,
    STATIC

}
