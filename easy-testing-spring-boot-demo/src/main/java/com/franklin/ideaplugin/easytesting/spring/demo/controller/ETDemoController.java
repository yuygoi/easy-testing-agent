package com.franklin.ideaplugin.easytesting.spring.demo.controller;

import com.franklin.ideaplugin.easytesting.common.entity.ETRsp;
import com.franklin.ideaplugin.easytesting.common.entity.MethodInvokeData;
import com.franklin.ideaplugin.easytesting.controllerclient.utils.ControllerUtil;
import com.franklin.ideaplugin.easytesting.spring.config.MyServerProperties;
import com.franklin.ideaplugin.easytesting.spring.utils.SpringUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import javax.annotation.PostConstruct;
import java.lang.reflect.AnnotatedType;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.List;

/**
 * @author Ye Junhui
 * @since 2023/5/15
 */
@RestController
@RequestMapping("/etdemo")
@RequiredArgsConstructor
public class ETDemoController {

    private final RequestMappingHandlerMapping requestMappingHandlerMapping;
    private final MyServerProperties myServerProperties;

    @PostConstruct
    public void init() {
        requestMappingHandlerMapping.getHandlerMethods()
                .forEach((requestMappingInfo, handlerMethod) -> {
                    MethodParameter[] methodParameters = handlerMethod.getMethodParameters();
                    for (MethodParameter methodParameter : methodParameters) {
                        Parameter parameter = methodParameter.getParameter();
                        AnnotatedType annotatedType = parameter.getAnnotatedType();
                    }
                });
    }

    @GetMapping("/path/{id}")
    public void path(@PathVariable Long id) {

    }

    @PostMapping("/json")
    public void json(@RequestBody String json) {
        System.out.println(json);
    }

    @PostMapping("/form")
    public ETRsp<String> form(String key, Integer test) {
        System.out.println(key);
        System.out.println(test);
        System.out.println(RequestContextHolder.getRequestAttributes().getSessionId());
        return ETRsp.success(key);
    }

    @RequestMapping(value = "/formOld",method = RequestMethod.POST)
    public void formOld(@RequestParam("key") String key) throws Exception {
        System.out.println(RequestContextHolder.getRequestAttributes());
        String clientClazzName = ControllerUtil.getClientClazzName(ETDemoController.class);
        Class<?> clazz = Class.forName(clientClazzName);
        Object etDemoController = SpringUtil.getBean(clazz);
        Method form = clazz.getDeclaredMethod(
                ControllerUtil.getClientMethodName("formOld",0),
                MethodInvokeData.class,
                String.class,
                Method.class,
                String.class
        );
        form.setAccessible(true);
    }

//    public String testResult(String result){
//        if (result.equals("exception")){
//            throw new RuntimeException(result);
//        }
//        return result;
//    }


    @PostMapping("/list")
    public ETRsp<List<String>> list(@RequestParam List<String> brandIds) {
        brandIds.forEach(System.out::println);
        return ETRsp.success(brandIds);
    }
}
