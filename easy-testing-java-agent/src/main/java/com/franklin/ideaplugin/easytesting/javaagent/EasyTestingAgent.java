package com.franklin.ideaplugin.easytesting.javaagent;

import com.franklin.ideaplugin.easytesting.common.label.EasyTestingLabel;
import javassist.ClassPool;

import java.io.File;
import java.io.IOException;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.lang.instrument.Instrumentation;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.security.ProtectionDomain;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * @author Ye Junhui
 * @since 2023/7/6 22:03
 */
public class EasyTestingAgent {

    public static void premain(String agentOps, Instrumentation inst) throws IOException {
        List<JarFile> jarFiles = AgentUtils.resolveAgentOps(AgentUtils.getAgentOps());
        jarFiles.forEach(inst::appendToSystemClassLoaderSearch);
        EasyTestingLabel.print();
    }
}
