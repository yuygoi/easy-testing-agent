package com.franklin.ideaplugin.easytesting.spring.registry;

import com.franklin.ideaplugin.easytesting.spring.constants.StrPool;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author Ye Junhui
 * @since 2023/5/16
 */
@Data
public class SpringPortHolder {

    private final Integer port;

    public SpringPortHolder(Integer port) {
        this.port = port;
        if (port > 0) {
            System.setProperty(StrPool.EASY_TESTING_SERVER_PORT_KEY,String.valueOf(port));
        }
    }
}
