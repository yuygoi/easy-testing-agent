package com.franklin.ideaplugin.easytesting.spring.registry;

import com.franklin.ideaplugin.easytesting.core.registry.FileRegistry;
import com.franklin.ideaplugin.easytesting.spring.config.EasyTestingProperties;
import com.franklin.ideaplugin.easytesting.spring.utils.SpringUtil;

/**
 * @author Ye Junhui
 * @since 2023/5/16
 */
public class SpringFileRegistry{

    private final int port;
    private final EasyTestingProperties easyTestingProperties;

    public SpringFileRegistry(int port, EasyTestingProperties easyTestingProperties) {
        this.port = port;
        this.easyTestingProperties = easyTestingProperties;
    }

    public void doRegistry(){
        FileRegistry.registryServer(SpringUtil.getAppName(),this.port);
    }
}
