package com.franklin.ideaplugin.easytesting.spring.constants;

/**
 * @author Ye Junhui
 * @since 2023/5/20
 */
public interface StrPool {

    /**
     * 内部请求路径（用于触发请求头）
     */
    String INNER_REQUEST_PATH = "/easy-testing-execute";

    /**
     * 内部请求类型
     */
    String CONTENT_TYPE = "application/json";

    /**
     * easy-testing服务端口号key
     */
    String EASY_TESTING_SERVER_PORT_KEY = "easy-testing.server.port";
}
