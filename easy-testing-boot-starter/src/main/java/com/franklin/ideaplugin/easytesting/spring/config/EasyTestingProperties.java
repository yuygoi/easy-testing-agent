package com.franklin.ideaplugin.easytesting.spring.config;

import com.franklin.ideaplugin.easytesting.common.log.LoggerFactory;
import lombok.Data;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author Ye Junhui
 * @since 2023/5/18
 */
@EnableConfigurationProperties(EasyTestingProperties.class)
@ConfigurationProperties(prefix = "easy-testing")
@Data
@Configuration
public class EasyTestingProperties implements InitializingBean{

    /**
     * 是否开启
     */
    private Boolean enable = Boolean.TRUE;

    /**
     * 日志配置
     */
    private LogProperties log = new LogProperties();

    @Override
    public void afterPropertiesSet() throws Exception {
        //注册配置
        LoggerFactory.setLogEnable(this.log.enable);
    }

    /**
     * 日志配置
     */
    @Data
    public static class LogProperties{

        /**
         * 是否开启
         */
        private Boolean enable = Boolean.TRUE;

    }
}
