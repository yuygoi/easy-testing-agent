package com.franklin.ideaplugin.easytesting.spring.web.config;

import com.franklin.ideaplugin.easytesting.spring.config.EasyTestingAutoConfiguration;
import com.franklin.ideaplugin.easytesting.spring.invoke.SpringMethodInvoker;
import com.franklin.ideaplugin.easytesting.spring.web.controller.EasyTestingWebMethodController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author Ye Junhui
 * @since 2023/6/24 15:42
 */
@Configuration
@Import(EasyTestingAutoConfiguration.class)
public class EasyTestingWebAutoConfiguration {

    @Bean
    @Autowired
    public EasyTestingWebMethodController easyTestingControllerMethodController(
            SpringMethodInvoker springMethodInvoker
    ){
        return new EasyTestingWebMethodController(springMethodInvoker);
    }
}
