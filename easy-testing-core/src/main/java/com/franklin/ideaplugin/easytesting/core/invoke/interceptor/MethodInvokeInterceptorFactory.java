package com.franklin.ideaplugin.easytesting.core.invoke.interceptor;

import com.franklin.ideaplugin.easytesting.common.entity.MethodInvokeData;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;

/**
 * @author Ye Junhui
 * @since 2023/8/3
 */
public class MethodInvokeInterceptorFactory implements IMethodInvokeInterceptor {

    private final static MethodInvokeInterceptorFactory INSTANCE = new MethodInvokeInterceptorFactory();

    private final List<IMethodInvokeInterceptor> methodInvokeInterceptorList = Arrays.asList(
            new GenericParamTypeMethodInvokeInterceptor(),
            new ScriptMethodInvokeInterceptor()
    );

    @Override
    public void intercept(MethodInvokeData methodInvokeData, Type[] paramTypes, Object[] paramValues) {
        for (IMethodInvokeInterceptor methodInvokeInterceptor : this.methodInvokeInterceptorList) {
            methodInvokeInterceptor.intercept(methodInvokeData, paramTypes, paramValues);
        }
    }

    public static MethodInvokeInterceptorFactory getInstance() {
        return INSTANCE;
    }
}
