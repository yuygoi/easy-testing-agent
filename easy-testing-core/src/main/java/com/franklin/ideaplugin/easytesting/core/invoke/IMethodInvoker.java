package com.franklin.ideaplugin.easytesting.core.invoke;


import com.franklin.ideaplugin.easytesting.common.entity.MethodInvokeData;

import java.lang.reflect.Method;

/**
 * @author Ye Junhui
 * @since 2023/5/12
 */
public interface IMethodInvoker {

    /**
     * 执行方法
     *
     * @param methodInvokeData
     * @param targetClass
     * @param targetMethod
     * @param paramTypes
     * @param params
     * @throws Exception
     * @return
     */
    Object invoke(MethodInvokeData methodInvokeData, Class<?> targetClass, Method targetMethod, Class<?>[] paramTypes, Object[] params) throws Exception;
}
