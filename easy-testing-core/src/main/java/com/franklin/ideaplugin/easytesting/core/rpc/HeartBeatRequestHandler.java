package com.franklin.ideaplugin.easytesting.core.rpc;

import com.franklin.ideaplugin.easytesting.common.entity.HeartBeatData;

/**
 * 心跳检测，用于判断服务是否已下线
 *
 * @author Ye Junhui
 * @since 2023/5/24
 */
public class HeartBeatRequestHandler implements INettyHttpRequestHandler<HeartBeatData, String> {
    @Override
    public String getRequestUrl() {
        return "/heartbeat";
    }

    @Override
    public String handleRequest(HeartBeatData heartBeatData) throws Throwable {
        return "success";
    }
}
