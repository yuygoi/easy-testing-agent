package com.franklin.ideaplugin.easytesting.core.invoke;

import com.franklin.ideaplugin.easytesting.core.invoke.interceptor.IScriptObjectPropertyFiller;

import java.util.ArrayList;
import java.util.List;

/**
 * 方法调用注册中心
 *
 * @author Ye Junhui
 * @since 2023/5/15
 */
public class MethodInvokerRegistry {

    private static List<IMethodInvoker> methodInvokerList = new ArrayList<>();
    private static List<IScriptObjectPropertyFiller> scriptObjectPropertyFillerList = new ArrayList<>();

    /**
     * 添加方法调用者
     * @param methodInvoker
     */
    public static void addMethodInvoker(IMethodInvoker methodInvoker){
        methodInvokerList.add(methodInvoker);
    }

    public static List<IMethodInvoker> getMethodInvokerList(){
        return methodInvokerList;
    }

    public static List<IScriptObjectPropertyFiller> getScriptObjectPropertyFillerList() {
        return scriptObjectPropertyFillerList;
    }

    public static void addScriptObjectPropertyFillerList(IScriptObjectPropertyFiller scriptObjectPropertyFiller) {
        scriptObjectPropertyFillerList.add(scriptObjectPropertyFiller);
    }
}
