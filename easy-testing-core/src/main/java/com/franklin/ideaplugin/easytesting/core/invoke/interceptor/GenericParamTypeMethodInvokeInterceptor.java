package com.franklin.ideaplugin.easytesting.core.invoke.interceptor;

import cn.hutool.core.lang.Pair;
import com.franklin.ideaplugin.easytesting.common.entity.MethodInvokeData;
import com.franklin.ideaplugin.easytesting.common.entity.MethodParameter;
import com.franklin.ideaplugin.easytesting.common.utils.JsonUtils;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Franklin
 * @since 2023/08/01 21:35
 */
public class GenericParamTypeMethodInvokeInterceptor implements IMethodInvokeInterceptor {
    @Override
    public void intercept(MethodInvokeData methodInvokeData, Type[] paramTypes, Object[] paramValues) {
        List<Pair<String, MethodParameter>> paramList = methodInvokeData.getParameterMap().entrySet().stream()
                .map(entry -> Pair.of(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());
        for (int i = 0; i < paramList.size(); i++) {
            Pair<String, MethodParameter> param = paramList.get(i);
            String paramName = param.getKey();
            String jsonString = param.getValue().getValue();
            Type paramType = paramTypes[i];
            if (paramType instanceof ParameterizedType){
                paramValues[i] = JsonUtils.parseObject(jsonString,paramType);
            }
        }
    }
}
