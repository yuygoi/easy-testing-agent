package com.franklin.ideaplugin.easytesting.core.registry;

/**
 * @author Ye Junhui
 * @since 2023/6/23 22:36
 */
public class JdkFileRegistry {

    public final static String JAVA_VERSION = "java.version";
    public final static String JAVA_HOME = "java.home";
    public final static String JAVA_LIB = "src.zip";

    public static String getJavaVersion(){
        return System.getProperty(JAVA_VERSION);
    }

    public static String getJavaHome(){
        return System.getProperty(JAVA_HOME).replace('\\','/');
    }

    public static void registryJdk(String appName,int port){
        String javaVersion = getJavaVersion();
        if (javaVersion.startsWith("1.8")){
            registryJdk8(appName, port);
            return;
        }
        registryJdk11(appName, port);
    }

    /**
     * jdk8
     * @param appName
     * @param port
     */
    public static void registryJdk8(String appName,int port){
        String javaHome = getJavaHome();
        javaHome = javaHome.replace("/jre","");
        String path = javaHome + "/" + JAVA_LIB;
        FileRegistry.registryServerForLib(appName,path,port);
    }

    /**
     * >= jdk11
     * @param appName
     * @param port
     */
    public static void registryJdk11(String appName,int port){
        String javaHome = getJavaHome();
        String path = javaHome + "/lib/" + JAVA_LIB;
        FileRegistry.registryServerForLib(appName,path,port);
    }
}
