package com.franklin.ideaplugin.easytesting.core.rpc;


import com.franklin.ideaplugin.easytesting.common.utils.ReflectionUtils;

/**
 * @author Ye Junhui
 * @since 2023/5/15
 */
public interface INettyHttpRequestHandler<I,O> {

    /**
     * 请求路径
     * @return
     */
    String getRequestUrl();

    /**
     * 处理请求
     * @param i
     * @return
     */
    O handleRequest(I i) throws Throwable;

    /**
     * 输入数据类型
     * @return
     */
    default Class<?> getInputType(){
        return ReflectionUtils.getSuperInterfaceGenericType(this.getClass(),0,0);
    }

    /**
     * 输出数据类型
     * @return
     */
    default Class<?> getOutputType(){
        return ReflectionUtils.getSuperInterfaceGenericType(this.getClass(),0,1);
    }
}
