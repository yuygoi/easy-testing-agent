package com.franklin.ideaplugin.easytesting.core.invoke.interceptor;

import com.franklin.ideaplugin.easytesting.common.entity.MethodInvokeData;

import java.lang.reflect.Type;

/**
 * @author Ye Junhui
 * @since 2023/7/28
 */
public interface IMethodInvokeInterceptor {

    /**
     * 对方法执行参数进行拦截处理
     * @param methodInvokeData
     * @param paramTypes
     * @param paramValues
     */
    void intercept(MethodInvokeData methodInvokeData, Type[] paramTypes, Object[] paramValues);

}
