package com.franklin.ideaplugin.easytesting.core.invoke;


import com.franklin.ideaplugin.easytesting.common.entity.MethodInvokeData;
import com.franklin.ideaplugin.easytesting.common.utils.MethodUtils;

import java.lang.reflect.Method;

/**
 * @author Ye Junhui
 * @since 2023/5/21
 */
public class StaticMethodInvoker implements IMethodInvoker {
    @Override
    public Object invoke(MethodInvokeData methodInvokeData, Class<?> targetClass, Method targetMethod, Class<?>[] paramTypes, Object[] params) throws Exception {
        if (!MethodUtils.isStaticMethod(targetMethod)){
            return null;
        }
        return targetMethod.invoke(targetClass,params);
    }
}
