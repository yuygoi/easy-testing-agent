package com.franklin.ideaplugin.easytesting.core.thread;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author Ye Junhui
 * @since 2023/5/15
 */
public class EasyTestingThreadPool {

    /**
     * 服务器线程池
     */
    private final static ThreadPoolExecutor serverThreadPool = new ThreadPoolExecutor(
            1,
            2,
            60L,
            TimeUnit.SECONDS,
            new LinkedBlockingQueue<>(2000),
            r -> {
                Thread thread = new Thread(r, "ET-S-" + r.hashCode());
                thread.setDaemon(true);
                return thread;
            },
            (r, executor) -> {
                throw new RuntimeException("Easy-Testing, serverThreadPool is EXHAUSTED!");
            });

    /**
     * 业务线程池
     */
    private final static ThreadPoolExecutor bizThreadPool = new ThreadPoolExecutor(
            4,
            16,
            60L,
            TimeUnit.SECONDS,
            new LinkedBlockingQueue<>(2000),
            r -> new Thread(r, "ET-B-" + r.hashCode()),
            (r, executor) -> {
                throw new RuntimeException("Easy-Testing, bizThreadPool is EXHAUSTED!");
            });

    public static ThreadPoolExecutor getBizThreadPool(){
        return bizThreadPool;
    }

    public static ThreadPoolExecutor getServerThreadPool() {
        return serverThreadPool;
    }

    public static void shutDownAll(){
        serverThreadPool.shutdownNow();
        bizThreadPool.shutdownNow();
    }
}
