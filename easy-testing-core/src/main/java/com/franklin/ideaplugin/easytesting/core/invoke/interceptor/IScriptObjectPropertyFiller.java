package com.franklin.ideaplugin.easytesting.core.invoke.interceptor;

/**
 * @author Ye Junhui
 * @since 2023/8/1
 */
public interface IScriptObjectPropertyFiller {

    /**
     * 填充
     * @param scriptObject
     */
    void fill(Object scriptObject);
}
