package com.franklin.ideaplugin.easytesting.common.resolver;

import com.franklin.ideaplugin.easytesting.common.entity.MethodParameter;
import com.franklin.ideaplugin.easytesting.common.resolver.commontype.*;
import com.franklin.ideaplugin.easytesting.common.resolver.datetype.DateMethodParameterResolver;
import com.franklin.ideaplugin.easytesting.common.resolver.javatype.*;
import com.franklin.ideaplugin.easytesting.common.resolver.javawrappertype.*;
import com.franklin.ideaplugin.easytesting.common.resolver.jsontype.ArrayMethodParameterResolver;
import com.franklin.ideaplugin.easytesting.common.resolver.jsontype.ObjectMethodParameterResolver;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ye Junhui
 * @since 2023/7/5
 */
public class MethodParameterResolverFactory {

    private static final List<IMethodParameterResolver> methodParameterResolverList;

    static {
        methodParameterResolverList = new ArrayList<>();
        methodParameterResolverList.add(new JavaBooleanMethodParameterResolver());
        methodParameterResolverList.add(new JavaByteMethodParameterResolver());
        methodParameterResolverList.add(new JavaCharMethodParameterResolver());
        methodParameterResolverList.add(new JavaDoubleMethodParameterResolver());
        methodParameterResolverList.add(new JavaFloatMethodParameterResolver());
        methodParameterResolverList.add(new JavaIntMethodParameterResolver());
        methodParameterResolverList.add(new JavaLongMethodParameterResolver());
        methodParameterResolverList.add(new JavaShortMethodParameterResolver());
        methodParameterResolverList.add(new BooleanMethodParameterResolver());
        methodParameterResolverList.add(new ByteMethodParameterResolver());
        methodParameterResolverList.add(new CharacterMethodParameterResolver());
        methodParameterResolverList.add(new DoubleMethodParameterResolver());
        methodParameterResolverList.add(new FloatMethodParameterResolver());
        methodParameterResolverList.add(new IntegerMethodParameterResolver());
        methodParameterResolverList.add(new LongMethodParameterResolver());
        methodParameterResolverList.add(new ShortMethodParameterResolver());
        methodParameterResolverList.add(new ClassMethodParameterResolver());
        methodParameterResolverList.add(new EnumMethodParameterResolver());
        methodParameterResolverList.add(new DateMethodParameterResolver());
        methodParameterResolverList.add(new ChildClassMethodParameterResolver());
        methodParameterResolverList.add(new FileClassMethodParameterResolver());
        methodParameterResolverList.add(new StringMethodParameterResolver());
        methodParameterResolverList.add(new ArrayMethodParameterResolver());
        methodParameterResolverList.add(new ObjectMethodParameterResolver());
    }

    /**
     * 获取解析器
     * @param methodParameter
     * @return
     */
    public static IMethodParameterResolver getResolver(MethodParameter methodParameter){
        for (IMethodParameterResolver methodParameterResolver : methodParameterResolverList) {
            if (methodParameterResolver.isTarget(methodParameter)){
                return methodParameterResolver;
            }
        }
        return null;
    }
}
