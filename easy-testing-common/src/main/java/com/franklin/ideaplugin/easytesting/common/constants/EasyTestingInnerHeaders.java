package com.franklin.ideaplugin.easytesting.common.constants;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Ye Junhui
 * @since 2023/6/30
 */
public interface EasyTestingInnerHeaders {


    /**
     * traceId
     */
    String TRACE_ID = "et-trace-id";

    /**
     * ET内部请求头
     */
    Set<String> ET_INNER_HEADERS = new LinkedHashSet<>(Arrays.asList(
            TRACE_ID
    ));

    static void clearEtInnerHeaders(Map<String,String> headers){
        ET_INNER_HEADERS.forEach(headers::remove);
    }
}
