package com.franklin.ideaplugin.easytesting.common.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Ye Junhui
 * @since 2023/7/4
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DateClass {

    /**
     * 类全称
     */
    private String qualifiedName;

    /**
     * 文件路径
     */
    private String datePattern;

    /**
     * 日期信息
     */
    private String dateValue;
}
