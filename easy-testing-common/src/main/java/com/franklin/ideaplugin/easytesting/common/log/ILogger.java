package com.franklin.ideaplugin.easytesting.common.log;

/**
 * @author Ye Junhui
 * @since 2023/5/31
 */
public interface ILogger {
    /**
     * info日志
     * @param format
     * @param args
     */
    void info(String format, Object... args);

    /**
     * debug日志
     * @param format
     * @param args
     */
    void debug(String format, Object... args);

    /**
     * error日志
     * @param format
     * @param args
     */
    void error(String format, Object... args);

    /**
     * error日志
     * @param message
     * @param throwable
     */
    void error( String message, Throwable throwable);
}
