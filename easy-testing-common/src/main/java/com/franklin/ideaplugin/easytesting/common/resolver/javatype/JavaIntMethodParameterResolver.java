package com.franklin.ideaplugin.easytesting.common.resolver.javatype;

import cn.hutool.core.util.StrUtil;
import com.franklin.ideaplugin.easytesting.common.entity.MethodParameter;
import com.franklin.ideaplugin.easytesting.common.resolver.IMethodParameterResolver;
import com.franklin.ideaplugin.easytesting.common.utils.MethodUtils;
import com.franklin.ideaplugin.easytesting.common.utils.ReflectionUtils;

/**
 * @author Ye Junhui
 * @since 2023/7/3
 */
public class JavaIntMethodParameterResolver implements IMethodParameterResolver {
    @Override
    public boolean isTarget(MethodParameter methodParameter) {
        String classQualifiedName = methodParameter.getClassQualifiedName();
        return classQualifiedName.equals("int");
    }

    @Override
    public Class<?> resolveType(MethodParameter methodParameter) {
        if (isTarget(methodParameter)){
            return int.class;
        }
        return null;
    }

    @Override
    public Object resolveValue(MethodParameter methodParameter) {
        if (StrUtil.isBlank(methodParameter.getValue())){
            return getDefaultValue(methodParameter);
        }
        return new Integer(methodParameter.getValue());
    }

    @Override
    public Object getDefaultValue(MethodParameter methodParameter) {
        return 0;
    }

}
