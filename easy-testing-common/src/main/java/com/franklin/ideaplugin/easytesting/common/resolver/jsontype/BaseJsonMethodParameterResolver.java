package com.franklin.ideaplugin.easytesting.common.resolver.jsontype;

import cn.hutool.core.util.StrUtil;
import com.franklin.ideaplugin.easytesting.common.entity.MethodParameter;
import com.franklin.ideaplugin.easytesting.common.resolver.IMethodParameterResolver;
import com.franklin.ideaplugin.easytesting.common.utils.JsonUtils;
import com.franklin.ideaplugin.easytesting.common.utils.ReflectionUtils;

import java.util.Objects;

/**
 * @author Ye Junhui
 * @since 2023/7/4
 */
public abstract class BaseJsonMethodParameterResolver implements IMethodParameterResolver {

    @Override
    public Object resolveValue(MethodParameter methodParameter) {
        if (StrUtil.isBlank(methodParameter.getValue())){
            return getDefaultValue(methodParameter);
        }
        Class<?> resolveType = resolveType(methodParameter);
        if (Objects.nonNull(resolveType)){
            if (!resolveType.isEnum()) {
                return JsonUtils.parseObject(methodParameter.getValue(),resolveType);
            }
        }
        return null;
    }

    @Override
    public Object getDefaultValue(MethodParameter methodParameter) {
        return null;
    }
}
