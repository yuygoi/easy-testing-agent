package com.franklin.ideaplugin.easytesting.common.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.LinkedHashMap;

/**
 * @author Ye Junhui
 * @since 2023/5/12
 */
@Data
public class MethodInvokeData implements Serializable {

    /**
     * 方法所在类的全类名
     */
    private String classQualifiedName;

    /**
     * 方法名
     */
    private String methodName;

    /**
     * 是否是枚举实例方法
     */
    private boolean isEnumInstanceMethod;

    /**
     * 枚举对象
     */
    private String enumTarget;

    /**
     * 参数列表
     */
    private LinkedHashMap<String,MethodParameter> parameterMap = new LinkedHashMap<>();

    /**
     * 请求头
     */
    private LinkedHashMap<String,String> headerMap = new LinkedHashMap<>();

    /**
     * 执行结果路径
     */
    private String executePath;

}
