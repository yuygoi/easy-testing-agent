package com.franklin.ideaplugin.easytesting.common.resolver.commontype;

import com.franklin.ideaplugin.easytesting.common.entity.MethodParameter;
import com.franklin.ideaplugin.easytesting.common.resolver.IMethodParameterResolver;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Ye Junhui
 * @since 2023/7/4
 */
public class ClassMethodParameterResolver implements IMethodParameterResolver {


    @Override
    public boolean isTarget(MethodParameter methodParameter) {
        String classQualifiedName = methodParameter.getClassQualifiedName();
        return classQualifiedName.equals("java.lang.Class");
    }

    @Override
    public Class<?> resolveType(MethodParameter methodParameter) {
        if (isTarget(methodParameter)){
            return Class.class;
        }
        return null;
    }

    @Override
    public Object resolveValue(MethodParameter methodParameter) {
        try {
            return Class.forName(methodParameter.getValue());
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public Object getDefaultValue(MethodParameter methodParameter) {
        return null;
    }
}
