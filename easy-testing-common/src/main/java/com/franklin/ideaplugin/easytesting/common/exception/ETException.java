package com.franklin.ideaplugin.easytesting.common.exception;

/**
 * @author Ye Junhui
 * @since 2023/6/20
 */
public class ETException extends RuntimeException{

    public ETException() {
    }

    public ETException(String message) {
        super(message);
    }

    public ETException(String message, Throwable cause) {
        super(message, cause);
    }

    public ETException(Throwable cause) {
        super(cause);
    }
}
