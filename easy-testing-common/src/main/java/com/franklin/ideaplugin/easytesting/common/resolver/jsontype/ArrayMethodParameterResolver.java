package com.franklin.ideaplugin.easytesting.common.resolver.jsontype;

import com.franklin.ideaplugin.easytesting.common.entity.MethodParameter;
import com.franklin.ideaplugin.easytesting.common.resolver.IMethodParameterResolver;
import com.franklin.ideaplugin.easytesting.common.utils.JsonUtils;
import com.franklin.ideaplugin.easytesting.common.utils.ReflectionUtils;

import java.util.Date;
import java.util.Objects;

/**
 * @author Ye Junhui
 * @since 2023/7/4
 */
public class ArrayMethodParameterResolver extends BaseJsonMethodParameterResolver {
    @Override
    public boolean isTarget(MethodParameter methodParameter) {
        return ReflectionUtils.isArrayClass(methodParameter.getClassQualifiedName());
    }

    @Override
    public Class<?> resolveType(MethodParameter methodParameter) {
        if (isTarget(methodParameter)){
            try {
                return ReflectionUtils.getArrayClass(methodParameter.getClassQualifiedName());
            } catch (ClassNotFoundException e) {
                return null;
            }
        }
        return null;
    }

}
