package com.franklin.ideaplugin.easytesting.common.resolver.commontype;

import cn.hutool.core.util.EnumUtil;
import com.franklin.ideaplugin.easytesting.common.entity.MethodParameter;
import com.franklin.ideaplugin.easytesting.common.resolver.IMethodParameterResolver;

import java.util.Objects;

/**
 * @author Ye Junhui
 * @since 2023/7/4
 */
public class EnumMethodParameterResolver implements IMethodParameterResolver {


    @Override
    public boolean isTarget(MethodParameter methodParameter) {
        try {
            Class<?> clazz = Class.forName(methodParameter.getClassQualifiedName());
            return clazz.isEnum();
        } catch (ClassNotFoundException e) {
            return false;
        }
    }

    @Override
    public Class<?> resolveType(MethodParameter methodParameter) {
        if (isTarget(methodParameter)){
            try {
                return Class.forName(methodParameter.getClassQualifiedName());
            } catch (Exception e) {

            }
        }
        return null;
    }

    @Override
    public Object resolveValue(MethodParameter methodParameter) {
        Class<?> resolveType = resolveType(methodParameter);
        if (Objects.nonNull(resolveType)){
            return EnumUtil.fromString((Class<? extends Enum>)resolveType,methodParameter.getValue());
        }
        return null;
    }

    @Override
    public Object getDefaultValue(MethodParameter methodParameter) {
        return null;
    }
}
