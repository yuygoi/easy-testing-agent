package com.franklin.ideaplugin.easytesting.common.utils;

import cn.hutool.core.util.SystemPropsUtil;

import java.util.Locale;

/**
 * @author Ye Junhui
 * @since 2023/7/9 8:52
 */
public class OsUtils {

    /**
     * 判断是否是Windows系统
     * @return
     */
    public static boolean isWindows(){
        return SystemPropsUtil.get("os.name","windows").toLowerCase(Locale.ROOT).contains("windows");
    }
}
