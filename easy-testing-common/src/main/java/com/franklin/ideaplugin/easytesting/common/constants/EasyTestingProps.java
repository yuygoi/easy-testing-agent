package com.franklin.ideaplugin.easytesting.common.constants;

/**
 * @author Ye Junhui
 * @since 2023/5/31
 */
public interface EasyTestingProps {

    /**
     * 是否开启日志
     */
    String LOG_ENABLE = "easy-testing.log.enable";
}
