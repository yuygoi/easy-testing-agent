package com.franklin.ideaplugin.easytesting.common.utils;


import cn.hutool.core.io.FastByteArrayOutputStream;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

/**
 * @author Ye Junhui
 * @since 2023/6/22 14:06
 */
public class ExceptionUtils {

    /**
     * 将异常转成字符串
     * @param throwable
     * @return
     */
    public static String exceptionToString(Throwable throwable){
        FastByteArrayOutputStream out = new FastByteArrayOutputStream();
        throwable.printStackTrace(new PrintStream(out));
        return out.toString();
    }
}
