package com.franklin.ideaplugin.easytesting.common.resolver.javawrappertype;

import cn.hutool.core.util.StrUtil;
import com.franklin.ideaplugin.easytesting.common.entity.MethodParameter;
import com.franklin.ideaplugin.easytesting.common.resolver.IMethodParameterResolver;

/**
 * @author Ye Junhui
 * @since 2023/7/3
 */
public class ByteMethodParameterResolver implements IMethodParameterResolver {
    @Override
    public boolean isTarget(MethodParameter methodParameter) {
        String classQualifiedName = methodParameter.getClassQualifiedName();
        return classQualifiedName.equals("java.lang.Byte");
    }

    @Override
    public Class<?> resolveType(MethodParameter methodParameter) {
        if (isTarget(methodParameter)){
            return Byte.class;
        }
        return null;
    }

    @Override
    public Object resolveValue(MethodParameter methodParameter) {
        if (StrUtil.isBlank(methodParameter.getValue())){
            return null;
        }
        return Byte.valueOf(methodParameter.getValue());
    }

    @Override
    public Object getDefaultValue(MethodParameter methodParameter) {
        return 0;
    }

}
