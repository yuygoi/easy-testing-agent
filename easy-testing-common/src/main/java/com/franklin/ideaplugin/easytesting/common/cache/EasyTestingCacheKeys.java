package com.franklin.ideaplugin.easytesting.common.cache;

/**
 * @author Franklin
 * @since 2023/08/03 22:17
 */
public interface EasyTestingCacheKeys {

    /**
     * 脚本类
     */
    String SCRIPT_CLASS = "script.class";
}
