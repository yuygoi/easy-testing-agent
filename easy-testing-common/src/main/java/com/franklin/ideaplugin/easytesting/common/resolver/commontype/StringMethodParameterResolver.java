package com.franklin.ideaplugin.easytesting.common.resolver.commontype;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import com.franklin.ideaplugin.easytesting.common.entity.MethodParameter;
import com.franklin.ideaplugin.easytesting.common.resolver.IMethodParameterResolver;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Ye Junhui
 * @since 2023/7/4
 */
public class StringMethodParameterResolver implements IMethodParameterResolver {

    private final static Map<String,Class<?>> STRING_CLASSES;

    static {
        STRING_CLASSES = new HashMap<>();
        STRING_CLASSES.put(String.class.getName(),String.class);
        STRING_CLASSES.put(CharSequence.class.getName(),CharSequence.class);
    }

    @Override
    public boolean isTarget(MethodParameter methodParameter) {
        String classQualifiedName = methodParameter.getClassQualifiedName();
        return STRING_CLASSES.containsKey(classQualifiedName);
    }

    @Override
    public Class<?> resolveType(MethodParameter methodParameter) {
        if (isTarget(methodParameter)){
            String classQualifiedName = methodParameter.getClassQualifiedName();
            return STRING_CLASSES.getOrDefault(classQualifiedName,String.class);
        }
        return null;
    }

    @Override
    public Object resolveValue(MethodParameter methodParameter) {
        if (StrUtil.isBlank(methodParameter.getValue())){
            return getDefaultValue(methodParameter);
        }
        return methodParameter.getValue();
    }

    @Override
    public Object getDefaultValue(MethodParameter methodParameter) {
        return null;
    }
}
