package com.franklin.ideaplugin.easytesting.common.entity;

import lombok.Data;

/**
 * @author Ye Junhui
 * @since 2023/5/25
 */
@Data
public class HeartBeatData {
    private String message;
}
