package com.franklin.ideaplugin.easytesting.common.utils;

import cn.hutool.core.net.URLDecoder;
import cn.hutool.core.util.SystemPropsUtil;

import java.nio.charset.StandardCharsets;

/**
 * @author Ye Junhui
 * @since 2023/5/27 17:14
 */
public class MyPathUtil {

    /**
     * 去除路径不允许的字符
     * @param path
     * @return
     */
    public static String deleteErrorChars(String path){
        return path
                .replace('<','(')
                .replace('>',')')
                .replace(":","")
                .replace('\\','/');
    }

    /**
     * 获取包分割符
     * @return
     */
    public static String getLibSeparator(){
        if (OsUtils.isWindows()){
            return ";";
        }else {
            return ":";
        }
    }

    public static int getIdeaPrefixIndex(){
        if (OsUtils.isWindows()){
            return 1;
        }else {
            return 0;
        }
    }

    /**
     * 基本路径
     * @return
     */
    public static String getCacheBasePath(String cache){
        String projectBasePath = System.getProperty("user.dir");
        projectBasePath = URLDecoder.decodeForPath(projectBasePath, StandardCharsets.UTF_8).replace('\\','/');
        return getEtServerPath(projectBasePath) + cache;
    }

    public static String getEtServerPath(String projectBasePath){
        return projectBasePath + "/.idea/etServer/";
    }
}
