package com.franklin.ideaplugin.easytesting.common.resolver.jsontype;

import com.franklin.ideaplugin.easytesting.common.entity.MethodParameter;
import com.franklin.ideaplugin.easytesting.common.utils.ReflectionUtils;

/**
 * @author Ye Junhui
 * @since 2023/7/4
 */
public class ObjectMethodParameterResolver extends BaseJsonMethodParameterResolver {
    @Override
    public boolean isTarget(MethodParameter methodParameter) {
        return !ReflectionUtils.isArrayClass(methodParameter.getClassQualifiedName());
    }

    @Override
    public Class<?> resolveType(MethodParameter methodParameter) {
        if (isTarget(methodParameter)){
            try {
                return Class.forName(methodParameter.getClassQualifiedName());
            } catch (ClassNotFoundException e) {
                return null;
            }
        }
        return null;
    }

}
