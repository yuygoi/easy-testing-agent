package com.franklin.ideaplugin.easytesting.common.entity;

import cn.hutool.core.util.StrUtil;
import com.franklin.ideaplugin.easytesting.common.utils.JsonUtils;
import com.franklin.ideaplugin.easytesting.common.utils.ReflectionUtils;
import lombok.Data;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author Ye Junhui
 * @since 2023/5/15
 */
@Data
public class MethodParameter implements Serializable {

    private String classQualifiedName;

    private String value;

    private String rewriteValue;

    public String getStringValue(){
        if (StrUtil.isBlank(value)){
            return null;
        }
        if (classQualifiedName.equals(FileClass.class.getCanonicalName())){
            FileClass fileClass = JsonUtils.parseObject(value, FileClass.class);
            if (Objects.isNull(fileClass)){
                return null;
            }
            return fileClass.getFilePath();
        }
        if (classQualifiedName.equals(DateClass.class.getCanonicalName())){
            DateClass dateClass = JsonUtils.parseObject(value, DateClass.class);
            if (Objects.isNull(dateClass)){
                return null;
            }
            return dateClass.getDateValue();
        }
        if (classQualifiedName.equals(ChildClass.class.getCanonicalName())){
            ChildClass childClass = JsonUtils.parseObject(value, ChildClass.class);
            if (Objects.isNull(childClass)){
                return null;
            }
            return childClass.getChildValue();
        }

        if (StrUtil.isNotBlank(rewriteValue)){
            return rewriteValue;
        }

        return value;
    }
}
