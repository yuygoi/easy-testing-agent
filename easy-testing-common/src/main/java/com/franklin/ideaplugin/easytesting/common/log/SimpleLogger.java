package com.franklin.ideaplugin.easytesting.common.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Ye Junhui
 * @since 2023/5/31
 */
public class SimpleLogger implements ILogger {

    private final Class<?> CLASS;
    /**
     * 是否开启日志
     */
    private final boolean isLogEnable;

    private final Logger log;

    public SimpleLogger(Class<?> clazz, boolean isLogEnable) {
        CLASS = clazz;
        this.isLogEnable = isLogEnable;
        this.log = LoggerFactory.getLogger(CLASS);
    }


    /**
     * info
     *
     * @param format
     * @param args
     */
    @Override
    public void info(String format, Object... args) {
        if (isLogEnable) {
            log.info(format, args);
        }
    }

    @Override
    public void debug(String format, Object... args) {
        if (isLogEnable) {
            log.debug(format, args);
        }
    }

    @Override
    public void error(String format, Object... args) {
        if (isLogEnable) {
            log.error(format, args);
        }
    }

    @Override
    public void error(String message, Throwable throwable) {
        if (isLogEnable) {
            log.error(message, throwable);
        }
    }

}
