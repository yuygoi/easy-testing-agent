package com.franklin.ideaplugin.easytesting.common.entity;

import lombok.Data;

import java.util.LinkedHashMap;

/**
 * 子类
 *
 * @author Ye Junhui
 * @since 2023/7/14
 */
@Data
public class ChildClass {

    /**
     * 父类名
     */
    private String superClassName;

    /**
     * 子类名
     */
    private String childClassName;

    /**
     * 子类值
     */
    private String childValue;
}
