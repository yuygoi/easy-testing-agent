package com.franklin.ideaplugin.easytesting.common.utils;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.net.URLDecoder;
import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.SystemPropsUtil;

import java.io.File;
import java.nio.charset.StandardCharsets;

/**
 * @author Ye Junhui
 * @since 2023/7/13
 */
public class EnvUtils {

    /**
     * 判断是否是有效的环境
     * @return
     */
    public static boolean isValidEnv(){
        String basePath = SystemPropsUtil.get("user.dir", "");
        if (StrUtil.isBlank(basePath)){
            return false;
        }
        basePath = URLDecoder.decodeForPath(basePath, StandardCharsets.UTF_8).replace('\\','/');
        File file = FileUtil.file(basePath + "/.idea");
        return file.exists() && file.isDirectory();
    }
}
