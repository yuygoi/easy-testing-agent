package com.franklin.ideaplugin.easytesting.common.log;

import cn.hutool.core.util.SystemPropsUtil;
import com.franklin.ideaplugin.easytesting.common.constants.EasyTestingProps;

/**
 * @author Ye Junhui
 * @since 2023/5/31
 */
public class LoggerFactory {

    /**
     * 设置可用
     * @param enable
     */
    public static void setLogEnable(boolean enable){
        //注册配置
        SystemPropsUtil.set(EasyTestingProps.LOG_ENABLE,String.valueOf(enable));
    }

    /**
     * 获取logger
     * @return
     */
    public static ILogger getLogger(Class<?> clazz){
        return new SimpleLogger(clazz,SystemPropsUtil.getBoolean(EasyTestingProps.LOG_ENABLE,true));
    }
}
