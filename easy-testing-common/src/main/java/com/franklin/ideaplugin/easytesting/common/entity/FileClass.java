package com.franklin.ideaplugin.easytesting.common.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Ye Junhui
 * @since 2023/7/2 16:51
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FileClass {

    /**
     * 类全称
     */
    private String qualifiedName;

    /**
     * 文件路径
     */
    private String filePath;
}
