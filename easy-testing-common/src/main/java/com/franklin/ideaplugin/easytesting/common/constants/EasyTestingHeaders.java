package com.franklin.ideaplugin.easytesting.common.constants;

import java.util.*;

/**
 * @author Ye Junhui
 * @since 2023/6/30
 */
public interface EasyTestingHeaders {

    /**
     * 是否是http请求
     */
    String IS_HTTP_REQUEST = "et-is-http-request";

    /**
     * http请求url重写
     */
    String HTTP_URL_REWRITE = "et-http-url-write";

    /**
     * http请求参数重写
     */
    String HTTP_PARAM_REWRITE = "et-http-param-write";

    /**
     * ET请求头
     */
    Set<String> ET_HEADERS = new LinkedHashSet<>(Arrays.asList(
            IS_HTTP_REQUEST,
            HTTP_URL_REWRITE,
            HTTP_PARAM_REWRITE
    ));

    static void clearEtHeaders(Map<String,String> headers){
        ET_HEADERS.forEach(headers::remove);
    }
}
