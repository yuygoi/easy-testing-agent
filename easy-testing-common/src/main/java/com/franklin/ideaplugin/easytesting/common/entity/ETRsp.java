package com.franklin.ideaplugin.easytesting.common.entity;

import com.franklin.ideaplugin.easytesting.common.utils.ExceptionUtils;
import lombok.Data;

import java.io.Serializable;

/**
 * @author Ye Junhui
 * @since 2023/5/15
 */
@Data
public class ETRsp<T> implements Serializable {

    private String code;

    private String msg;

    private T data;

    public static <T> ETRsp<T> success(T data){
        ETRsp<T> tetRsp = new ETRsp<T>();
        tetRsp.setData(data);
        tetRsp.setCode("200");
        tetRsp.setMsg("success");
        return tetRsp;
    }

    public static <T> ETRsp<T> fail(T data){
        ETRsp<T> tetRsp = new ETRsp<T>();
        tetRsp.setData(data);
        tetRsp.setCode("500");
        tetRsp.setMsg("fail");
        return tetRsp;
    }

    public static ETRsp<Object> fail(Throwable throwable){
        return fail(ExceptionUtils.exceptionToString(throwable));
    }
}
