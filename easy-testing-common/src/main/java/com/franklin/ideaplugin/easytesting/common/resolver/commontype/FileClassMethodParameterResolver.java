package com.franklin.ideaplugin.easytesting.common.resolver.commontype;

import com.franklin.ideaplugin.easytesting.common.entity.ChildClass;
import com.franklin.ideaplugin.easytesting.common.entity.FileClass;
import com.franklin.ideaplugin.easytesting.common.entity.MethodParameter;
import com.franklin.ideaplugin.easytesting.common.resolver.IMethodParameterResolver;
import com.franklin.ideaplugin.easytesting.common.utils.JsonUtils;

import java.util.Objects;

/**
 * @author Ye Junhui
 * @since 2023/7/17
 */
public class FileClassMethodParameterResolver implements IMethodParameterResolver {
    @Override
    public boolean isTarget(MethodParameter methodParameter) {
        return methodParameter.getClassQualifiedName().equals(FileClass.class.getCanonicalName());
    }

    @Override
    public Class<?> resolveType(MethodParameter methodParameter) {
        if (isTarget(methodParameter)){
            FileClass fileClass = JsonUtils.parseObject(methodParameter.getValue(), FileClass.class);
            if (Objects.nonNull(fileClass)){
                try {
                    return Class.forName(fileClass.getQualifiedName());
                } catch (ClassNotFoundException e) {
                    //ignore
                }
            }
        }
        return null;
    }

    @Override
    public Object resolveValue(MethodParameter methodParameter) {
        return null;
    }

    @Override
    public Object getDefaultValue(MethodParameter methodParameter) {
        return null;
    }
}
