package com.franklin.ideaplugin.easytesting.common.exception;

import com.franklin.ideaplugin.easytesting.common.log.ILogger;
import com.franklin.ideaplugin.easytesting.common.log.LoggerFactory;

/**
 * @author Ye Junhui
 * @since 2023/6/20
 */
public class ETAssert {

    private static final ILogger log = LoggerFactory.getLogger(ETAssert.class);

    public static void isTrue(boolean condition,String message){
        if (!condition){
            log.info(message);
            throw new ETException(message);
        }
    }
}
