package com.franklin.ideaplugin.easytesting.common.cache;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * @author Franklin
 * @since 2023/08/03 22:15
 */
public class EasyTestingCache {

    private final static ThreadLocal<String> TRACE_ID_HOLDER = new ThreadLocal<>();

    private final static ConcurrentMap<String, Map<String,Object>> CACHE = new ConcurrentHashMap<>();

    public static void cache(String traceId,String key,Object value){
        Map<String, Object> map = CACHE.get(traceId);
        if (CollectionUtil.isEmpty(map)){
            map = new HashMap<>();
        }
        map.put(key, value);
        CACHE.put(traceId,map);
    }

    public static Object getValue(String traceId,String key){
        if (StrUtil.isBlank(traceId)){
            return null;
        }
        Map<String, Object> map = CACHE.get(traceId);
        if (CollectionUtil.isEmpty(map)){
            return null;
        }
        return map.get(key);
    }

    public static void setTraceId(String traceId){
        TRACE_ID_HOLDER.set(traceId);
    }

    public static String getTraceId(){
        return TRACE_ID_HOLDER.get();
    }

    public static void clear(String traceId){
        TRACE_ID_HOLDER.remove();
        CACHE.remove(traceId);
    }
}
