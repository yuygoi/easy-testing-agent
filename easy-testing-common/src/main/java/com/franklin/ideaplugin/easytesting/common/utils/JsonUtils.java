package com.franklin.ideaplugin.easytesting.common.utils;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONWriter;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.franklin.ideaplugin.easytesting.common.entity.ETRsp;
import com.franklin.ideaplugin.easytesting.common.log.ILogger;
import com.franklin.ideaplugin.easytesting.common.log.LoggerFactory;

import java.lang.reflect.Type;

/**
 * @author Ye Junhui
 * @since 2023/5/22
 */
public class JsonUtils {

    private static final ILogger log = LoggerFactory.getLogger(JsonUtils.class);

    private final static ObjectMapper objectMapper = new ObjectMapper()
            .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

    public static <T> T parseObject(String jsonString,Type type){

        try {
            return JSON.parseObject(jsonString,type);
        } catch (Exception e) {
            return null;
        }
    }

    public static String toJSONString(Object object){
        try {
            return objectMapper.writeValueAsString(object);
        } catch (Exception e) {
            return "";
        }
    }


}
