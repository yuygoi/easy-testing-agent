package com.franklin.ideaplugin.easytesting.common.resolver;

import com.franklin.ideaplugin.easytesting.common.entity.MethodParameter;

/**
 * 方法参数解析器
 *
 * @author Ye Junhui
 * @since 2023/7/3
 */
public interface IMethodParameterResolver {

    /**
     * 是否是对象
     * @param methodParameter
     * @return
     */
    boolean isTarget(MethodParameter methodParameter);

    /**
     * 解析参数类型
     * @param methodParameter
     * @return
     */
    Class<?> resolveType(MethodParameter methodParameter);

    /**
     * 解析参数值
     * @param methodParameter
     * @return
     */
    Object resolveValue(MethodParameter methodParameter);

    /**
     * 获取默认值
     * @param methodParameter
     * @return
     */
    Object getDefaultValue(MethodParameter methodParameter);
}
