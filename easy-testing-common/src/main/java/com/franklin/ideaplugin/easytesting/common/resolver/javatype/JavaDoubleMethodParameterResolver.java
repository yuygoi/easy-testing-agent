package com.franklin.ideaplugin.easytesting.common.resolver.javatype;

import cn.hutool.core.util.StrUtil;
import com.franklin.ideaplugin.easytesting.common.entity.MethodParameter;
import com.franklin.ideaplugin.easytesting.common.resolver.IMethodParameterResolver;

/**
 * @author Ye Junhui
 * @since 2023/7/3
 */
public class JavaDoubleMethodParameterResolver implements IMethodParameterResolver {
    @Override
    public boolean isTarget(MethodParameter methodParameter) {
        String classQualifiedName = methodParameter.getClassQualifiedName();
        return classQualifiedName.equals("double");
    }

    @Override
    public Class<?> resolveType(MethodParameter methodParameter) {
        if (isTarget(methodParameter)){
            return double.class;
        }
        return null;
    }

    @Override
    public Object resolveValue(MethodParameter methodParameter) {
        if (StrUtil.isBlank(methodParameter.getValue())){
            return getDefaultValue(methodParameter);
        }
        return new Double(methodParameter.getValue());
    }

    @Override
    public Object getDefaultValue(MethodParameter methodParameter) {
        return 0.0d;
    }

}
