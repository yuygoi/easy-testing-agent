package com.franklin.ideaplugin.easytesting.common.label;

/**
 * @author Ye Junhui
 * @since 2023/6/3
 */
public final class EasyTestingLabel {

    private final static String LABEL = "\n" +
            "\n" +
            " ____  ____  ____  ____  ____  ____  ____  ____  ____  ____  ____  ____ \n" +
            "||E ||||a ||||s ||||y ||||- ||||T ||||e ||||s ||||t ||||i ||||n ||||g ||\n" +
            "||__||||__||||__||||__||||__||||__||||__||||__||||__||||__||||__||||__||\n" +
            "|/__\\||/__\\||/__\\||/__\\||/__\\||/__\\||/__\\||/__\\||/__\\||/__\\||/__\\||/__\\|\t\tEasy Testing Is Intercepting" +
            "\n"
            ;

    public static void print(){
        System.out.println(LABEL);
    }

}
