package com.franklin.ideaplugin.easytesting.spring.webflux.config;

import com.franklin.ideaplugin.easytesting.spring.config.EasyTestingAutoConfiguration;
import com.franklin.ideaplugin.easytesting.spring.invoke.SpringMethodInvoker;
import com.franklin.ideaplugin.easytesting.spring.webflux.controller.EasyTestingWebfluxMethodController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author Ye Junhui
 * @since 2023/6/24 15:42
 */
@Configuration
@Import(EasyTestingAutoConfiguration.class)
public class EasyTestingWebFluxAutoConfiguration {

    @Bean
    @Autowired
    public EasyTestingWebfluxMethodController easyTestingControllerMethodController(
            SpringMethodInvoker springMethodInvoker
    ){
        return new EasyTestingWebfluxMethodController(springMethodInvoker);
    }
}
