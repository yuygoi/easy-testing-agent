package com.franklin.ideaplugin.easytesting.compiler.constant;

/**
 * @author Ye Junhui
 * @since 2023/7/28
 */
public interface CompilerConstants {

    /**
     * 包名
     */
    String PACKAGE_NAME = "com.franklin.ideaplugin.easytesting.script";

    /**
     * 请求头方法名
     */
    String HEADER_METHOD_NAME = "easyTestingHeaders";
}
