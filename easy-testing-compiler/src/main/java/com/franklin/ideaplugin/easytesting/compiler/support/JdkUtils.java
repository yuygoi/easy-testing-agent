package com.franklin.ideaplugin.easytesting.compiler.support;

/**
 * @author Franklin
 * @since 2023/07/28 23:34
 */
public class JdkUtils {

    private final static String JAVA_VERSION = "java.version";

    /**
     * 获取java版本号
     * @return
     */
    public static String getJavaVersion(){
        String javaVersion = System.getProperty(JAVA_VERSION);
        if (javaVersion.startsWith("1.")){
            return javaVersion.substring(0,3);
        }
        String[] split = javaVersion.split("\\.");
        return split[0];
    }

}
