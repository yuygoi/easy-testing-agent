package com.franklin.ideaplugin.easytesting.compiler;

/**
 * 动态编译器
 *
 * @author Ye Junhui
 * @since 2023/7/27
 */
public interface ICompiler {

    /**
     * 编译 java 源码
     *
     * @param code        java 源代码
     * @param classLoader 类加载器
     * @return 编译后的class
     */
    Class<?> compile(String code, ClassLoader classLoader);
}
